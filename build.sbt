import com.typesafe.sbt.packager.docker.ExecCmd

name := """pollster"""
organization := "dev.giger"

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.5"

libraryDependencies ++= Seq(
  guice,
  evolutions,
  jdbc,
  "org.postgresql" % "postgresql" % "42.2.16",
  "org.playframework.anorm" %% "anorm" % "2.6.10",
  "org.playframework.anorm" %% "anorm-postgres" % "2.6.10",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "dev.giger.controllers._"

// Adds additional packages into conf/routes
play.sbt.routes.RoutesKeys.routesImport += "model._"

enablePlugins(DockerPlugin)
dockerBaseImage := "openjdk:11-jre"
dockerExposedPorts := Seq(9000)
dockerAlias := DockerAlias(Some("registry.heroku.com"), Some("gigerdo-pollster"), "web", Some("latest"))
dockerEntrypoint := Seq()
dockerCommands += ExecCmd("CMD", "bin/pollster")
