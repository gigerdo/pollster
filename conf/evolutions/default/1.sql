-- polls schema

-- !Ups
CREATE TABLE Poll
(
    id   text PRIMARY KEY,
    data jsonb
);

-- !Downs
DROP TABLE Poll;