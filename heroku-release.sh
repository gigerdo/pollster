#!/bin/bash
# Call heroku api to trigger a redeployment of a new image
# (See https://devcenter.heroku.com/articles/container-registry-and-runtime)
# Takes the image-id of the local image and triggers redeployment. This image must be pushed to the heroku registry.
imageId=$(docker inspect registry.heroku.com/gigerdo-pollster/web --format={{.Id}})
payload='{"updates":[{"type":"web","docker_image":"'"$imageId"'"}]}'
curl -n -X PATCH https://api.heroku.com/apps/gigerdo-pollster/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_API_KEY"