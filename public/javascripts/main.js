$(document).on("keydown", "input:last", event => {
    var keyCode = event.keyCode || event.which;
    if (keyCode === 9 && !event.shiftKey) {
        event.preventDefault();
        console.log("focusout", event);
        addNewInput();
    }
})

let i = 3;

function addNewInput() {
    console.log("Adding new input");
    $(".form-group").last().after(`
<div class="form-group ">
    <input type="text" id="answers_${i}" name="answers[${i}]" value="">
    <span class="errors"></span>
    <span class="help"></span>
</div>`);
    $("input").last().focus();
    i += 1;
}