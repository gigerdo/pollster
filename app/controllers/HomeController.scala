package controllers

import anorm._
import anorm.postgresql._
import model._
import play.api.data.Form
import play.api.data.Forms.{list, mapping, nonEmptyText, text}
import play.api.db.Database
import play.api.libs.json.{JsValue, Json, OWrites, Writes}
import play.api.mvc._

import java.sql.Connection
import javax.inject._
import scala.language.postfixOps

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents, database: Database) extends BaseController with play.api.i18n.I18nSupport {
  private val form = Form(
    mapping(
      "question" -> nonEmptyText,
      "answers" -> list(text)
        .transform(l => l.map(text => text.strip()).filter(text => text.nonEmpty), (l: List[String]) => l)
        .verifying("Write at least two answers", l => l.length >= 2)
        .verifying("Too many answers (Maximum of 100)", l => l.length <= 100)
    )(CreatePollData.apply)(CreatePollData.unapply)
  )

  private val voteForm = Form(
    mapping(
      "answerId" -> nonEmptyText
    )(VotePollData.apply)(VotePollData.unapply)
  )

  private implicit val pollAnswerIdWrites: Writes[PollAnswerId] = Json.valueWrites[PollAnswerId]
  private implicit val pollIdWrites: Writes[PollId] = Json.valueWrites[PollId]
  private implicit val pollAnswerWrites: OWrites[PollAnswer] = Json.writes[PollAnswer]
  private implicit val pollWrites: OWrites[Poll] = Json.writes[Poll]

  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(form, routes.HomeController.createPoll()))
  }

  def createPoll: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val errorFunction = { formWithErrors: Form[CreatePollData] =>
      // This is the bad case, where the form had validation errors.
      // Let's show the user the form again, with the errors highlighted.
      // Note how we pass the form with errors to the template.
      BadRequest(views.html.index(formWithErrors, routes.HomeController.createPoll()))
    }

    val successFunction = { data: CreatePollData =>
      val answers = data.answers.map(text => PollAnswer(text, 0))
      val poll = Poll(data.question, answers)
      database.withConnection { implicit connection =>
        SQL("insert into poll(id, data) values({id}, {data})")
          .on("id" -> poll.id.id, "data" -> Json.toJson(poll))
          .executeUpdate()
      }
      Redirect(routes.HomeController.showPoll(poll.id))
    }

    val formValidationResult = form.bindFromRequest()
    formValidationResult.fold(errorFunction, successFunction)
  }

  def showPoll(id: PollId): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    database.withConnection { implicit connection =>
      val result = getPoll(id)
        .map(poll => views.html.vote(poll, voteForm, routes.HomeController.placeVote(id)))
        .getOrElse(views.html.error())
      Ok(result)
    }
  }

  def placeVote(id: PollId): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    database.withTransaction { implicit connection =>
      getPoll(id)
        .map(poll => {
          val errorFunction = { formWithErrors: Form[VotePollData] =>
            // This is the bad case, where the form had validation errors.
            // Let's show the user the form again, with the errors highlighted.
            // Note how we pass the form with errors to the template.
            BadRequest(views.html.vote(poll, formWithErrors, routes.HomeController.placeVote(id)))
          }

          val successFunction = { data: VotePollData =>
            val newAnswers = poll.answers.map(answer => {
              if (answer.id == new PollAnswerId(data.answerId)) {
                answer.copy(votes = answer.votes + 1)
              } else {
                answer
              }
            })
            val newPoll = poll.copy(answers = newAnswers)

            SQL("update poll set data={data} where id={id}")
              .on("id" -> poll.id.id, "data" -> Json.toJson(newPoll))
              .executeUpdate()

            Redirect(routes.HomeController.showPollResults(id))
          }

          val formValidationResult = voteForm.bindFromRequest()
          formValidationResult.fold(errorFunction, successFunction)
        })
        .getOrElse(Ok(views.html.error()))
    }
  }

  def showPollResults(id: PollId): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    database.withConnection { implicit connection =>
      val result = getPoll(id)
        .map(poll => views.html.results(poll, poll.answers.map(a => a.votes).sum))
        .getOrElse(views.html.error())
      Ok(result)
    }
  }

  private def getPoll(id: PollId)(implicit conn: Connection): Option[Poll] = {
    SQL("select data from poll where id={id} for update").on("id" -> id.id)
      .as(SqlParser.get[JsValue]("data") *)
      .headOption
      .flatMap(data => {
        implicit val pollAnswerIdReads = Json.valueReads[PollAnswerId]
        implicit val pollIdReads = Json.valueReads[PollId]
        implicit val pollAnswerReads = Json.reads[PollAnswer]
        implicit val pollReads = Json.reads[Poll]
        Json.fromJson[Poll](data).asOpt
      })
  }
}
