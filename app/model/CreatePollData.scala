package model

case class CreatePollData(question: String, answers: List[String])
