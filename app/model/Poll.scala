package model

import java.util.UUID

class PollId(val id: String = UUID.randomUUID().toString) extends AnyVal

case class Poll(question: String, answers: List[PollAnswer], id: PollId = new PollId())

