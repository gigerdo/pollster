package model

import java.util.UUID

class PollAnswerId(val id: String = UUID.randomUUID().toString) extends AnyVal

case class PollAnswer(text: String, votes: Int, id: PollAnswerId = new PollAnswerId())
