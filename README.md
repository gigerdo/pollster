## Pollster

Simple strawpoll-like tool to create small polls.

Stack

- Scala
- SBT
- Play Framework

### Development

Start development database (Defaults: user: postgres, pw: postgres)

```bash
docker run --rm -d -p 5432:5432 -v postgres-data:/var/lib/postgresql/data postgres
```

Run dev server

```bash
sbt run
```

### Deployment on Heroku

Code is in `gitlab-ci.yml`

1. Build image
    ```bash
    sbt docker:publishLocal
    ```

2. Push to heroku registry (
   See: [Heroku Documentation](https://devcenter.heroku.com/articles/container-registry-and-runtime)). The name of the
   image must be in this exact format: registry.heroku.com/<app-slug>/<dyno-type>
    ```bash
    docker push registry.heroku.com/gigerdo-pollster/web
    ```

3. Pushing to registry will not trigger a redeployment. This has to be done either via CLI or via web
   API. `heroku-release.sh` contains a script to trigger the web API. Needs the heroku API-Key set as `HEROKU_API_KEY`
   env variable.
    ```bash
    HEROKU_API_KEY=yourkey ./heroku-release.sh
    ```

### TODO

- Nicer ids
- Create custom build image with sbt, scala and docker-cli